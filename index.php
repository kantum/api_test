<?php
// You have to set this before running the bot
//$apikey='xxx';
//$apisecret='xxx';

function bittrex_balance($apikey, $apisecret, $symbol)
{
	$nonce=time();
	$uri = 'https://bittrex.com/api/v1.1/account/getbalance?apikey='.$apikey.'&currency='.$symbol.'&nonce='.$nonce;
	$sign = hash_hmac('sha512',$uri,$apisecret);
	$ch = curl_init($uri);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$execResult = curl_exec($ch);
	$obj = json_decode($execResult, true);
	$balance = $obj["result"]["Available"];
	return $balance;
}

$cmcap = "https://api.coinmarketcap.com/v1/ticker/?limit=50";
$fgc = json_decode(file_get_contents($cmcap), true);

$balance = bittrex_balance($apikey, $apisecret, "BTC");
echo 'You have '.$balance.' BTC';
$balance = bittrex_balance($apikey, $apisecret, "RDD");
echo '<br>You have '.$balance.' RDD';
$counter = 0;
$mov = 2;
for ($i = 0; $i < 50; $i++)
{
	if($counter < 10)
	{
		$percCng = $fgc[$i]["percent_change_7d"];
		if ($percCng < $mov && $percCng > -$mov && $fgc[$i]["symbol"] != 'BTC')
		{
			$symbol = $fgc[$i]["symbol"];
			$cost = $fgc[$i]["price_btc"];
			$usd = $fgc[$i]["price_usd"];
			echo "<br>$symbol cost $cost BTC and $usd usd";
			$balance = bittrex_balance($apikey, $apisecret, "BTC");
			$counter++;
		}
	}
}
?>
